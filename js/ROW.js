	//assign the button event handler
	window.addEventListener('load', chooseAuthType );
	window.addEventListener('load', chooseAuthTypeJumpIP );
	document.getElementById( 'authType' ).addEventListener('change', chooseAuthType	);
	document.getElementById( 'authTypeJumpIP' ).addEventListener('change', chooseAuthTypeJumpIP	);
	document.getElementById( 'jumpips' ).addEventListener('input', setJumpIPAuthInfo );
	document.getElementById( 'wireshark' ).addEventListener( 'click', launchWireshark );
	document.getElementById( 'copy' ).addEventListener( 'click', copyToClipboard );
	document.getElementById( 'reload_command' ).addEventListener( 'click', syncCommand );
	
	//Set auth box correctly based on user selection
	function chooseAuthType() {
		if(document.getElementById( 'authType' ).value == "password") {
			document.getElementById("authContent").type = "password";
			document.getElementById("authContent").placeholder="Password";
		} else {
			document.getElementById("authContent").type = "text";
			document.getElementById("authContent").placeholder="Key location";
		}
	}
	
	function chooseAuthTypeJumpIP() {
		if(document.getElementById( 'authTypeJumpIP' ).value == "password") {
			document.getElementById("authContentJumpIP").type = "password";
			document.getElementById("authContentJumpIP").placeholder="Password";
		} else {
			document.getElementById("authContentJumpIP").type = "text";
			document.getElementById("authContentJumpIP").placeholder="Key location";
		}
		setJumpIPAuthInfo();
	}
	
	//Set up jump server auth if needed
	function setJumpIPAuthInfo() {
		if(document.getElementById( 'jumpips' ).value) {
			// document.getElementById( 'jumpipsinput' ).className = "form-group col-md-3";
			document.getElementById('jumpipsauth').removeAttribute("hidden");
		} else {
			// document.getElementById( 'jumpipsinput' ).className = "form-group col-md-6";
			document.getElementById( 'jumpipsauth' ).setAttribute("hidden", "true");
		}
	}
	
	function launchWireshark() {
		//the base url
		var url = 'Wireshark://';
		
		var command = buildFinalCommandString()
		
		//join all the parameters together and add to the url
		url += command;
		var a = document.createElement('a');
		a.href = url;
		document.body.appendChild(a);
		a.click();
	}
	
	function buildFinalCommandString() {
		// Create string that represents login details
		var connection = buildConnectionString();
		
		// Create string the represents interface
		var iface = buildInterfaceString();
		
		// Manage tcpdump flag logic
		var extraflags = buildExtraFlagsString();
		
		// Build command to run tcpdump and send into Wireshark
		var magic = connection + " tcpdump -U -s0 -w -" + " -i " + iface + extraflags + " | wireshark -k -i -";
		
		// Add command to kill tcpdump on remote server
		magic += " & " + connection + " pkill tcpdump";
		
		return magic;
	}
	
	// Stop script if missing one or more of the following: username, password or target ip
	function checkMissingInfo() {
		for(var arg = 0; arg < arguments.length; ++ arg)
		{
			if(!arguments[arg])
			{
				<!-- alert("Missing connection details (username/password/ip)"); -->
				swal.fire({
						title: "Missing Connection Details!",
						text: "Username, SSH key/Password, and Target IP are all required. Make sure you haven't forgotten other optional fields.",
						icon : "error",
				});
			throw new Error("Missing Connection Details!");
			}
		}
	}

	// Stop script if auth is set to something other than SSH key or password
	function blockBadAuth() {
		swal.fire({
				title: "Illegal authentication option!",
				text: "You may select only SSH key or Password for authentication method.",
				icon : "error",
		});
		throw new Error("Bad Authentication Option!");
	}

	function buildConnectionString() {
		var username = document.getElementById( 'username' ).value;
		var authContent = document.getElementById( 'authContent' ).value; // Password or key location for target ip
		var authContentJumpIP = document.getElementById("authContentJumpIP").value; // Password or key location for jump ip
		var targetip = document.getElementById( 'targetips' ).value;
		var jumpip = document.getElementById( 'jumpips' ).value;
		var authType = document.getElementById( 'authType' ).value;
		var usernameJumpIP = document.getElementById( 'usernameJumpIP' ).value;
		var authTypeJumpIP = document.getElementById( 'authTypeJumpIP' ).value;
				
		// Design connection command based on need for jump server
		if(jumpip){
			checkMissingInfo(username, authContent, authContentJumpIP, targetip);
			if(authTypeJumpIP == "sshkey") {
				authContentJumpTarget = "ssh -i " + authContentJumpIP;
			} else if(authTypeJumpIP == "password") {
				authContentJumpTarget = "plink -batch -ssh -pw " + authContentJumpIP;
			} else {
				blockBadAuth();
			}
			if(authType == "sshkey") {
				authContentTarget = "ssh -i " + authContent;
			} else if(authType == "password") {
				 authContentTarget = "sshpass -p " + authContent + " ssh";
			} else {
				blockBadAuth();
			}
			if(!usernameJumpIP) {
				usernameJumpIP = username // Set the username for the jump server to be the same as the target username
			}
			var connection = authContentJumpTarget + " " + usernameJumpIP +"@" + jumpip + " " + authContentTarget + " " + username + "@" + targetip;
		} else {
			checkMissingInfo(username, authContent, targetip);
			if(authType == "sshkey") {
				authContentTarget = "ssh -i " + authContent;
			} else if(authType == "password") {
				authContentTarget = "plink -batch -ssh -pw " + authContent;
			} else {
				blockBadAuth();
			}
			var connection = authContentTarget + " " + username +"@" + targetip;
		}
		return connection;
	}

	function buildInterfaceString() {
		var iface = document.getElementById( 'iface' ).value;
		if(iface){
			return iface;
		} else {
			return "any";
		}
	}
	
	function buildExtraFlagsString() {
		var host = document.getElementById( 'host' ).value;
		var port = document.getElementById( 'port' ).value;
		var extraflags = "";
		if(host){
			extraflags += " host " + host;
		}
		if(port){
			if(extraflags) { extraflags += " and " }
			extraflags += " port " + port;
		}
		return extraflags;
	}
	
	function syncCommand() {
		document.getElementById('command').value = buildFinalCommandString()
	}

	function copyToClipboard() {
		var copyText = document.getElementById('command');
		copyText.select();
		copyText.setSelectionRange(0, 99999)
		document.execCommand("copy");
	}
