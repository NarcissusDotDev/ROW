//== Class definition
var SweetAlert2 = function() {
	var init = function() {
		$('#alert_target_ips').click(function(e) {
			swal.fire({
				title: "Choose IP!",
				icon : "info",
				input: 'select',
				inputOptions: {
					'10.0.0.11': '10.0.0.11 - no-VirtualBox',
					'10.0.0.12': '10.0.0.12 - no-VirtualBox',
					'127.0.1.3': '127.0.1.3 - HOSTNAME13',
					'127.0.1.4': '127.0.1.4 - HOSTNAME14',
					'127.0.1.5': '127.0.1.5 - HOSTNAME15'
				},
				inputPlaceholder: 'Or any other',
				inputValidator: (value) => {
					return new Promise((resolve) => {
						targetips.value = value;
						resolve()
					})
				},
				onAfterClose: () => {
					document.getElementById("targetips").focus();
				},
			});
		});
		
		$('#alert_jump_ips').click(function(e) {
			swal.fire({
				title: "Choose IP!",
				icon : "info",
				input: 'select',
				inputOptions: {
					'10.0.0.14': '10.0.0.14 - no-VirtualHopBox',
					'127.0.0.2': '127.0.0.2 - HOSTNAME02',
					'127.0.0.3': '127.0.0.3 - HOSTNAME03',
					'127.0.0.4': '127.0.0.4 - HOSTNAME04',
					'127.0.0.5': '127.0.0.5 - HOSTNAME05'
				},
				inputPlaceholder: 'Or any other',
				inputValidator: (value) => {
					return new Promise((resolve) => {
						jumpips.value = value;
						setJumpIPAuthInfo();
						resolve()
					})
				},
				onAfterClose: () => {
					document.getElementById("jumpips").focus();
				},
			});
		});
		
		$('#alert_missing_connection_details').click(function(e) {
			swal.fire({
				title: "Oops...",
				text: "Missing Connection Details!",
				icon : "error",
				input: 'select',
			});
		});
	};

	return {
		//== Init
		init: function() {
			init();
		},
	};
}();

//== Class Initialization
jQuery(document).ready(function() {
	SweetAlert2.init();
});