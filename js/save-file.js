function saveTextAsFile(id)
{
	var textToSave = document.getElementById(id).innerText;
	var textToSaveAsBlob = new Blob([textToSave], {type:"text/" + id.slice(-3)});
	var textToSaveAsURL = window.URL.createObjectURL(textToSaveAsBlob);
	var fileNameToSaveAs = "ROW." + id.slice(-3);

	var downloadLink = document.createElement("a");
	downloadLink.download = fileNameToSaveAs;
	downloadLink.innerHTML = "Download File";
	downloadLink.href = textToSaveAsURL;
	downloadLink.style.display = "none";
	document.body.appendChild(downloadLink);
	downloadLink.click();
}